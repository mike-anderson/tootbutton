package ca.mike_anderson.tootbutton;

import java.util.Random;

/**
 * Created by Mike on 2017-04-22.
 */

class Euphemism {

    public String getEuphemism() {
        Random rand = new Random();
        int randomNumber = rand.nextInt(6);
        String euphemism;

        switch (randomNumber) {
            case 0:
                euphemism = "Crop has been dusted!";
                break;
            case 1:
                euphemism = "Bombs dropped!";
                break;
            case 2:
                euphemism = "Air biscuit baked!";
                break;
            case 3:
                euphemism = "Wind broken!";
                break;
            case 4:
                euphemism = "Cheese has been cut";
                break;
            default:
                euphemism = "Toot smacked!";
                break;
        }

        return euphemism;
    }


}
