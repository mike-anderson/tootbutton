package ca.mike_anderson.tootbutton;

import android.widget.ImageButton;
import android.support.v7.app.AppCompatActivity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class main extends AppCompatActivity {
    private static MediaPlayer mediaPlayer1;
    private static MediaPlayer mediaPlayer2;
    private static MediaPlayer mediaPlayer3;

    //initial playback state
    private static AudioState audioState1 = AudioState.STOPPED;
    private static AudioState audioState2 = AudioState.STOPPED;
    private static AudioState audioState3 = AudioState.STOPPED;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton tootButton1 = (ImageButton) findViewById(R.id.tootButton1);
        ImageButton tootButton2 = (ImageButton) findViewById(R.id.tootButton2);
        ImageButton tootButton3 = (ImageButton) findViewById(R.id.tootButton3);

        Toast.makeText(getApplicationContext(), "Enjoy!", Toast.LENGTH_SHORT).show();

        tootButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Button Clicked");

                if (mediaPlayer1 != null) {
                    mediaPlayer1.stop();
                    mediaPlayer1.release();
                    mediaPlayer1 = null;
                    audioState1 = AudioState.STOPPED;
                }

                mediaPlayer1 = new MediaPlayer();
                mediaPlayer1 = MediaPlayer.create(getApplicationContext(), R.raw.fart1);
                audioState1 = AudioState.PLAYBACK;
                mediaPlayer1.start();

                mediaPlayer1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mediaPlayer1 != null) {
                            audioState1 = AudioState.STOPPED;
                            /*Euphemism euphemism = new Euphemism();
                            Toast.makeText(getApplicationContext(), euphemism.getEuphemism(), Toast.LENGTH_SHORT).show();*/
                        }
                    }
                });
            }
        });

        tootButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Button Clicked");

                if (mediaPlayer2 != null) {
                    mediaPlayer2.stop();
                    mediaPlayer2.release();
                    mediaPlayer2 = null;
                    audioState2 = AudioState.STOPPED;
                }

                mediaPlayer2 = new MediaPlayer();
                mediaPlayer2 = MediaPlayer.create(getApplicationContext(), R.raw.fart2);
                audioState2 = AudioState.PLAYBACK;
                mediaPlayer2.start();

                mediaPlayer2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mediaPlayer2 != null) {
                            audioState2 = AudioState.STOPPED;
                            /*Euphemism euphemism = new Euphemism();
                            Toast.makeText(getApplicationContext(), euphemism.getEuphemism(), Toast.LENGTH_SHORT).show();*/
                        }
                    }
                });
            }
        });

        tootButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Button Clicked");

                if (mediaPlayer3 != null) {
                    mediaPlayer3.stop();
                    mediaPlayer3.release();
                    mediaPlayer3 = null;
                    audioState3 = AudioState.STOPPED;
                }

                mediaPlayer3 = new MediaPlayer();
                mediaPlayer3 = MediaPlayer.create(getApplicationContext(), R.raw.fart3);
                audioState3 = AudioState.PLAYBACK;
                mediaPlayer3.start();

                mediaPlayer3.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mediaPlayer3 != null) {
                            audioState3 = AudioState.STOPPED;
                            /*Euphemism euphemism = new Euphemism();
                            Toast.makeText(getApplicationContext(), euphemism.getEuphemism(), Toast.LENGTH_SHORT).show();*/
                        }
                    }
                });
            }
        });

    }
}
