package ca.mike_anderson.tootbutton;

/**
 * Created by Mike on 2017-04-14.
 */
public enum AudioState {
    STOPPED (0),
    PLAYBACK (1),
    RECORD (2);

    private int state;

    AudioState(int state) {
        this.state = state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setState(AudioState audioState) {
        this.state = audioState.getState();
    }

    public void setState(String audioStateAsString) {
        this.state = AudioState.valueOf(audioStateAsString).getState();
    }

    private int getState() {
        return state;
    }

    public int getState(String audioStateAsString) {
        return AudioState.valueOf(audioStateAsString).getState();
    }
}
